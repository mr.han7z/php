<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh String</h1>
    <?php
    $string = "PHP is never old";
    $panjangkalimat = strlen($string);
    $jumlahkata = str_word_count($string);
    
    echo "Kalimat : $string <br>";
    echo "Panjang String : $panjangkalimat <br>";
    echo "Jumlah Kata : $jumlahkata <br> ";

    echo "<h2> soal nomor 2 </h2>";
    $string1 = "I Love PHP";
    echo "Kalimat : $string1 <br>";
    echo "Kata pertama : ". substr($string1,0,1) . "<br>";
    echo "Kata kedua : ". substr ($string1,2,4) ."<br>";
    echo "Kata ketiga : ". substr ($string1, 6,4);
    
    echo "<h2> contoh ke tiga </h2>";
    $string2 ="Nama saya Farhan";
    echo "kalimat : $string2 <br>";
    echo "kalimat string diganti : " . str_replace("Farhan","mr.han7z", $string2 ."<br>");
    
    ?>
<?php 
$materi = "PHP";
echo ($materi == "PHP")? "Hari ini belajar PHP" : "Hari ini bukan materi PHP";
?>

</body>
</html>