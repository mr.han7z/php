<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2> Contoh Array </h2>
    <?php
            echo "<h3> Contoh Array 1 </h3>";
            $kids = array("Han","Ky","Ham " . "<br>" ."<br>");
            $adults = array("Farhan","Rizky","Ilham");
            print_r($kids);
            print_r($adults);

            echo "<h3> Contoh array 2 </h3>";
            echo "Total Array kids : ".count($kids);
            echo "<br>";
            echo "Total Array adults :" .count($adults);
            echo "<ul>";
            echo "<li>". $kids[0] . "</li>";
            echo "<li>". $kids[1] . "</li>";
            echo "<li>". $kids[2] . "</li>";
            echo "<br>";
            echo "<li>". $adults[0] . "</li>";
            echo "<li>". $adults[1] . "</li>";
            echo "<li>". $adults[2] . "</li>";
            
            echo "</ul>";
            
            echo "<h3> Contoh Array 3, array asosiatif";
            $biodata = [
                ["Nama" => "Farhan","Kota" => "Doloksanggul", "Umur" => 19],
                ["Nama" => "Doli", "Kota" =>"Barangan", "Umur" => 20],
                ["Nama" => "Fadhiil","Kota"=> "Pasar", "Umur" => 21],
            ];
            echo "<pre>";
            print_r($biodata);
            echo "</pre>";

    ?>
</body>
</html>