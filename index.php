<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    $sapa = "Halo Guys! <br> <br>";
    $hello ="Hello World! <br> <br>";
    // echo
    echo $hello;
    echo $sapa;
    // print
    print $hello;
  
    // print_r
    print_r($hello);

    // var_dump
    var_dump($hello);

  $jargon = "Tetap Santai dan Berkualitas";
  // akan menampilkan panjang variabel $jargon yaitu 28 karakter
  echo strlen($jargon); 
  
  // akan menampilkan panjang string 11
  echo strlen("Halo semua!");  
?>

<?php
$passwordasli = "belajarPHP";
$passwordinput = "belajarPHP";
$output = strcmp($passwordasli, $passwordinput);
if ($output !== 0)
  {
    echo "Password anda salah!";
  }
else
  {
    echo "Password anda benar.";
  }
?>


<?php
  $statement = "ini gak marah, cuman caps<br>";
  
  // menampilkan INI GAK MARAH, CUMAN CAPS
  echo strtoupper($statement);
?>

<?php
  $kalimat = "Saya sedang belajar PHP";
  $katayangdicari = "PHP";
  $posisi = strpos($kalimat, $katayangdicari);
  echo $posisi;
  echo $katayangdicari; 
?>
</body>
</html>